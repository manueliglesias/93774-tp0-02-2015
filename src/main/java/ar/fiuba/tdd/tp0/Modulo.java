package ar.fiuba.tdd.tp0;

public class Modulo implements IOperacion {

	@Override
	public String getOperador() {
		return "MOD";
	}

	@Override
	public float realizarOperacion(float operador1, float operador2) {
		return operador1 % operador2;
	}

}

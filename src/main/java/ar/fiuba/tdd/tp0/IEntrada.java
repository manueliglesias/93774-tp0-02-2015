package ar.fiuba.tdd.tp0;

import java.util.List;

public interface IEntrada{
	
	public void operarConPila(List<Numero> pila);
}

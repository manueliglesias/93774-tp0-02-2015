package ar.fiuba.tdd.tp0;

import java.util.List;

public class OperadorEneario implements IEntrada {

	private IOperacion operacion;
	
	public OperadorEneario(IOperacion _operacion) {
		operacion = _operacion;
	}
	
	@Override
	public void operarConPila(List<Numero> pila) {
		
		float resultado = pila.get(0).valor();
		// pila.size()-1 para evitar procesar el ultimo valor de la lista = NO-PROCESABLE
		for(int i = 1; i < pila.size()-1; i++){
			resultado = operacion.realizarOperacion(resultado, pila.get(i).valor());
		}
		
		pila.clear();
		pila.add(new Numero(resultado));
	}
}

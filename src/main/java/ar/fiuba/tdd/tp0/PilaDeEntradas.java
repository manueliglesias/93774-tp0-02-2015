package ar.fiuba.tdd.tp0;

import java.util.ArrayList;
import java.util.List;

public class PilaDeEntradas{
	private List<Numero> pila;
	
	public PilaDeEntradas(){
		pila = new ArrayList<Numero>();
		pila.add( new Numero("NO-PROCESABLE"));
	}
	
	public void meterEntrada(IEntrada entrada){
		entrada.operarConPila(pila);
	}
	
	public Float devolverResultado() {
		return pila.get(0).valor();
	}
}

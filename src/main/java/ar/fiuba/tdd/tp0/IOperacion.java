package ar.fiuba.tdd.tp0;

public interface IOperacion {
	
	public String getOperador();
	
	public float realizarOperacion(float operador1, float operador2);
}

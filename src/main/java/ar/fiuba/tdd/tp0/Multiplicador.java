package ar.fiuba.tdd.tp0;

public class Multiplicador implements IOperacion {

	@Override
	public float realizarOperacion(float operador1, float operador2) {
		return operador1 * operador2;		
	}

	@Override
	public String getOperador() {
		return "*";
	}
}

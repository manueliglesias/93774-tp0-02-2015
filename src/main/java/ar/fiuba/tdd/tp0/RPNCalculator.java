package ar.fiuba.tdd.tp0;

import java.util.StringTokenizer;

public class RPNCalculator {

    public float eval(String expression) {
    	
    	chequearNoNulo(expression);
    	
    	StringTokenizer tokenizador = new StringTokenizer(expression);
    	PilaDeEntradas pila = new PilaDeEntradas();
    	CreadorDeEntradas creador = new CreadorDeEntradas();    	
    	
    	creador.inicializar();
    	while(tokenizador.hasMoreTokens()){
    		IEntrada entrada = creador.crearEntrada(tokenizador.nextToken());
    		pila.meterEntrada(entrada);
    	}
    	
    	return pila.devolverResultado();
    }
    
    public void chequearNoNulo(Object expresion) {
    	
    	try{
    		expresion.toString();
    	}
    	catch(NullPointerException ex){
    		throw new IllegalArgumentException();
    	}
    }

}

package ar.fiuba.tdd.tp0;

public class CreadorDeEntradas {
	private MapaDeEntradas mapa;
	
	public void inicializar(){		
		mapa = new MapaDeEntradas();
		
		iniciarOperadoresBinarios();
		
		iniciarOperadoresEnearios();
	}
	
	public IEntrada crearEntrada(String token){
		IEntrada resultado = mapa.devolverEntrada(token);
		
		return resultado;
	}
	
	private void iniciarOperadoresBinarios(){
		mapa.agregarEntradaBinaria(new Sumador());
		mapa.agregarEntradaBinaria(new Restador());
		mapa.agregarEntradaBinaria(new Multiplicador());
		mapa.agregarEntradaBinaria(new Divisor());
		mapa.agregarEntradaBinaria(new Modulo());
	}
	
	private void iniciarOperadoresEnearios(){
		mapa.agregarEntradaEnearia(new Sumador());
		mapa.agregarEntradaEnearia(new Restador());
		mapa.agregarEntradaEnearia(new Multiplicador());
		mapa.agregarEntradaEnearia(new Divisor());
		
	}
}

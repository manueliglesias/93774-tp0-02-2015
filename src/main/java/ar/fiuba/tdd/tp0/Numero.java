package ar.fiuba.tdd.tp0;

import java.util.List;

public class Numero implements IEntrada {

	private String numero;
	
	public Numero(String token){
		numero = token;
	}
	
	public Numero(Float num){
		numero = num.toString();
	}
	
	public Float valor(){
		return Float.parseFloat(numero);
	}
	
	@Override
	public void operarConPila(List<Numero> pila){
		pila.add(0, this);
	}
}

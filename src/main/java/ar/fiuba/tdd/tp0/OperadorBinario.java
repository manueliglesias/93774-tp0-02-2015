package ar.fiuba.tdd.tp0;

import java.util.List;

public class OperadorBinario implements IEntrada {

	private IOperacion operacion;
	
	public OperadorBinario(IOperacion _operacion){
		operacion = _operacion;
	}
	
	@Override
	public void operarConPila(List<Numero> pila) {		
		float operador1 = pila.get(0).valor();
		float operador2 = pila.get(1).valor();
		
		float resultado = operacion.realizarOperacion(operador2, operador1);
		
		pila.remove(1);
		pila.remove(0);
		
		pila.add(0, new Numero(resultado));
	}
	
}

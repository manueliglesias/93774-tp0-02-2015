package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Map;

public class MapaDeEntradas {
	
	private Map<String, IEntrada> mapa;
	
	public MapaDeEntradas(){
		mapa = new HashMap<String, IEntrada>();
	}
	
	public IEntrada devolverEntrada(String token){
		return mapa.computeIfAbsent(token, k -> new Numero(k));
	}
	
	public void agregarEntradaBinaria(IOperacion entrada){
		mapa.put(entrada.getOperador(), new OperadorBinario(entrada));
	}
	
	public void agregarEntradaEnearia(IOperacion entrada){
		String operador = entrada.getOperador()+entrada.getOperador();
		
		mapa.put(operador, new OperadorEneario(entrada));
	}
}
